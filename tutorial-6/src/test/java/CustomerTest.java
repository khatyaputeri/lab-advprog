import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class CustomerTest {

    // TODO: Remove redundancy in setting up test fixture in each test methods
    // Hint: Make the test fixture into an instance variable
    private Movie movie;
    private Rental rent;

    @Before
    public void setUp() {
        movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);

        rent = new Rental(movie, 3);

    }

    @Test
    public void getName() {
        Customer customer = new Customer("Alice");

        assertEquals("Alice", customer.getName());
    }

    @Test
    public void statementWithSingleMovie() {
        Customer customer = new Customer("Alice");
        customer.addRental(rent);

        String result = customer.statement();
        String[] lines = result.split("\n");

        assertEquals(4, lines.length);
        assertTrue(result.contains("Amount owed is 3.5"));
        assertTrue(result.contains("1 frequent renter points"));
    }

    // TODO Implement me!
    @Test
    public void statementWithMultipleMovies() {
        Movie movie2 = new Movie("Alex, reborn", Movie.NEW_RELEASE);
        Movie movie3 = new Movie("Litte Alex", Movie.CHILDREN);

        Rental rent2 = new Rental(movie2, 4);
        Rental rent3 = new Rental(movie3, 1);
        Rental rent4 = new Rental(movie2, 1);
        Rental rent5 = new Rental(movie3, 4);
        Rental rent6 = new Rental(movie, 1);

        // TODO Implement me!
        Customer customer = new Customer("Khatya");
        customer.addRental(rent);
        customer.addRental(rent2);
        customer.addRental(rent3);
        customer.addRental(rent4);
        customer.addRental(rent5);
        customer.addRental(rent6);

        String result = customer.statement();
        String[] lines = result.split("\n");

        assertEquals(9, lines.length);
        assertTrue(result.contains("Amount owed is 25"));
        assertTrue(result.contains("7 frequent renter points"));

    }

    @Test
    public void htmlStatementTest() {

        Customer customer = new Customer("Kat");
        customer.addRental(rent);

        String result = customer.htmlStatement();
        String[] lines = result.split("\n");

        assertEquals(4, lines.length);
        assertTrue(result.contains("<P>You owe <EM>3.5</EM><P>"));
    }
}