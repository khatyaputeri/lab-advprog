package id.ac.ui.cs.advprog.tutorial1.strategy;

public class MallardDuck extends Duck {
    // TODO Complete me!

    public MallardDuck(){
        super.flyBehavior = new FlyWithWings();
        super.quackBehavior = new Quack();
    }

    public void display(){
        System.out.println("I look like a Mallard Duck");
    }
}
