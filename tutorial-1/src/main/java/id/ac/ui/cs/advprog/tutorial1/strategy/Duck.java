package id.ac.ui.cs.advprog.tutorial1.strategy;

public abstract class Duck {

    protected FlyBehavior flyBehavior;
    protected QuackBehavior quackBehavior;

    public void performFly() {
        flyBehavior.fly();
    }

    public void performQuack() {
        quackBehavior.quack();
    }

    // TODO Complete me!
    public void swim(){
        System.out.println("I'm swimming");

    }
    public abstract void display();

    public void setFlyBehavior(FlyBehavior newB){
        this.flyBehavior = newB;
    }

    public void setQuackBehavior(QuackBehavior newQ){
        this.quackBehavior = newQ;
    }



}
