package id.ac.ui.cs.advprog.tutorial1.strategy;

public class ModelDuck extends Duck{
    // TODO Complete me!

    public ModelDuck(){
        super.flyBehavior = new FlyNoWay();
        super.quackBehavior = new MuteQuack();
    }

    public void display(){
        System.out.println("I'm just a model");
    }


}
