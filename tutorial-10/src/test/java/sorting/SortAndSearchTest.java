package sorting;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class SortAndSearchTest {
    //TODO Implement, apply your test cases here
    private int[] arr1 = {1, 4, 2, 5, 3};
    private int[] arr2 = {2, 4, 1, 5, 3};
    private int[] arr3 = {3, 1, 2, 5, 4};

    @Test
    public void sortingTest() {
        Sorter.slowSort(arr1);
        Sorter.quickSort(arr2);

        assertEquals(3, arr1[2]);
        assertEquals(4, arr2[3]);
    }

    @Test
    public void finderTest() {
        int slow = Finder.slowSearch(arr3, 4);
        int fast = Finder.binSearch(arr3, 2);

        assertEquals(4, slow);
        assertEquals(2, fast);

    }

}
