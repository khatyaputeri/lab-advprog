package sorting;


public class Sorter {

    /**
     * Some sorting algorithm that possibly the slowest algorithm.
     *
     * @param inputArr array of integer that need to be sorted.
     * @return a sorted array of integer.
     */

    public static int[] slowSort(int[] inputArr) {
        int temp;
        for (int i = 1; i < inputArr.length; i++) {
            for (int j = i; j > 0; j--) {
                if (inputArr[j] < inputArr[j - 1]) {
                    temp = inputArr[j];
                    inputArr[j] = inputArr[j - 1];
                    inputArr[j - 1] = temp;
                }
            }
        }
        return inputArr;
    }

    /**
     * Better algorithm for sorting.
     *
     * @param inputArr array of integer that need to be sorted.
     * @return a sorted array of integer
     */

    public static int[] quickSort(int[] inputArr) {
        quickSort(inputArr, 0, inputArr.length - 1);
        return inputArr;
    }

    private static void quickSort(int[] inputArr, int start, int end) {
        int i = start;
        int j = end;
        int pivot = inputArr[start + (end - start) / 2];

        while (i <= j) {
            while (i < end && inputArr[i] < pivot) {
                i++;
            }
            while (j > start && inputArr[j] > pivot) {
                j--;
            }
            if (i <= j) {
                int temp = inputArr[i];
                inputArr[i] = inputArr[j];
                inputArr[j] = temp;

                i++;
                j--;
            }
        }
        int temp = inputArr[i];
        inputArr[i] = inputArr[end];
        inputArr[end] = temp;

        if (start < j) {
            quickSort(inputArr, start, j);
        }
        if (i < end) {
            quickSort(inputArr, i, end);
        }
    }

}
