import com.sun.org.apache.xpath.internal.SourceTree;

import java.util.Scanner;

public class Algoritma {

    public static void main(String[] args) {
        int fibs[] = new int[1000]; //inisiasi list untuk menyimpan semua bilangan fibonacci, maksimal fibonacci(999)
        fibs[0] = 0; //inisasi nilai awal
        fibs[1] = 1;
        fibs[2] = 1;

        Scanner scan = new Scanner(System.in);

        //input pertama untuk fungsi/soal nomor 1 (cek prima)
        int prime = Integer.parseInt(scan.nextLine());
        System.out.println(checkPrime(prime));

        //input kedua untuk fungsi/soal nomor 2 (hitung fibonacci)
        int fib = Integer.parseInt(scan.nextLine());
        System.out.println(fibonacci(fibs, fib));

        //input ketiga untuk fungsi/soal nomor 3(print sesuai urutan dalam desimal)
        String zeros = scan.nextLine();
        printZero(zeros);
    }

    public static void printZero(String num){
        num = num.replace(".", "");
        int x = Integer.parseInt(num);
        printZeroHelp(x);
    }

    public static void printZeroHelp(int num) {
        if((num % 10) == num) {
            System.out.println(num);
        } else {
            String temp = Integer.toString(num);
            int mod = (int) (num % Math.pow(10, temp.length()-1));
            System.out.println(num - mod);
            printZeroHelp(mod);
        }
    }

    public static boolean checkPrime(int num) {
        if(num > 2) {
            for(int i = 2; i < num/2; i++) {
                if(num % i == 0) {
                    return false;
                }
            }
            return true;
        }else if(num == 2) {
            return true;
        }
        return false;
    }

    //disimpan dalam list fibs agar dapat meningkatkan efisiensi
    public static int fibonacci(int[] fibs, int num) {
        if(num < 0){
            return 0;
        }else {
            if(fibs[num] == 0) {
                fibs[num] = fibonacci(fibs, num - 1) + fibonacci(fibs, num -2);
                return fibs[num];
            }
        }
        return fibs[num];
    }
}
