package game;

import java.util.concurrent.atomic.AtomicInteger;

public class Counter implements Runnable {
    //tidak tau cara untuk hitung lama waktu input dengan JAVA.TIMER
    //dimodelkan secara sederhana dengan atomic int
    private AtomicInteger countForScore;
    private AtomicInteger countForTime;

    public Counter() {
        this.countForScore = new AtomicInteger(0);
        this.countForTime = new AtomicInteger(0);
    }

    public synchronized void run() {
        try {
            Thread.sleep(1000);
            this.countForScore.getAndIncrement();
            this.countForTime.getAndIncrement();
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public synchronized AtomicInteger getCountForScore() {
        return countForScore;
    }

    public synchronized AtomicInteger getCountForTime() {
        return countForTime;
    }

    public synchronized int calculateAnsInsideThreshold(int score) {
        return (int) (score * 0.1); //add 10%
    }

    public synchronized int calculateAnsOutsideThreshold(int score) {
        return (int) (score * 0.05); //add 5%
    }

}
