package applicant;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.function.Predicate;

import org.junit.Before;
import org.junit.Test;

public class ApplicantTest {
    // TODO Implement me!
    // Increase code coverage in Applicant class
    // by creating unit test(s)!
    Applicant app;
    Predicate<Applicant> qualifiedEvaluator;
    Predicate<Applicant> creditEvaluator;
    Predicate<Applicant> employmentEvaluator;
    Predicate<Applicant> crimeCheck;

    @Before
    public void setUp() {

        app = new Applicant();

        qualifiedEvaluator = Applicant::isCredible;

        creditEvaluator = anApplicant -> anApplicant.getCreditScore() > 600;

        employmentEvaluator = anApplicant -> anApplicant.getEmploymentYears() > 0;

        crimeCheck = anApplicant -> !anApplicant.hasCriminalRecord();
    }

    @Test
    public void acceptedTest() {
        assertTrue(Applicant.evaluate(app, qualifiedEvaluator
                .and(creditEvaluator)));

        String msg = app.printEvaluation(Applicant.evaluate(app, qualifiedEvaluator
                .and(creditEvaluator)));
        assertEquals("Result of evaluating applicant: accepted", msg);
    }


    @Test
    public void rejecterdTest() {
        assertFalse(Applicant.evaluate(app, qualifiedEvaluator
                .and(employmentEvaluator).and(crimeCheck)));
    }
}
