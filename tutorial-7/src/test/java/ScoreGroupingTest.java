import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

public class ScoreGroupingTest {
    // TODO Implement me!
    // Increase code coverage in ScoreGrouping class
    // by creating unit test(s)!
    private Map<String, Integer> scores = new HashMap<>();
    private Map<Integer, List<String>> groupped = new HashMap<>();

    @Before
    public void setUp() {
        scores.put("Alice", 12);
        scores.put("Bob", 15);
        scores.put("Charlie", 11);
        scores.put("Delta", 15);
        scores.put("Emi", 15);
        scores.put("Foxtrot", 11);
        groupped = ScoreGrouping.groupByScores(scores);
    }

    @Test
    public void emptyTest() {
        assertEquals(null, groupped.get(13));
    }

    @Test
    public void groupSizeTest() {
        assertEquals(3, groupped.get(15).size());
    }

    @Test
    public void memberGroupTest() {
        assertTrue(groupped.get(11).contains("Charlie"));
    }

}