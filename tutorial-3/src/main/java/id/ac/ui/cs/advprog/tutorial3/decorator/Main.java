package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.ThickBunBurger;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.BeefMeat;

public class Main {
    public static void main(String[] args) {
        Food burger = new ThickBunBurger();
        burger = new BeefMeat(burger);
        System.out.println(burger.getDescription() + " cost $" + burger.cost());
    }
}
