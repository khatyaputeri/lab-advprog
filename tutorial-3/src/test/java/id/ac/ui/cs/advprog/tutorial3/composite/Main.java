package id.ac.ui.cs.advprog.tutorial3.composite;


import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.BackendProgrammer;

public class Main {
    public static void main(String[] args) {
        Employees emp = new BackendProgrammer("Khatya ", 200000);
        System.out.println(emp.getName() + " is a " + emp.getRole() + " with $" + emp.getSalary() + " salary");
    }
}
