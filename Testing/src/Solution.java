import com.sun.org.apache.xpath.internal.SourceTree;

import java.io.*;
import java.math.*;
import java.text.*;
import java.util.*;
import java.util.regex.*;

public class Solution {

    private static final Scanner scan = new Scanner(System.in);

    public static void main(String[] args) {
        int n = Integer.parseInt(scan.nextLine().trim());

        int[] t = new int[n];

        String[] tItems = scan.nextLine().split(" ");

        for (int tItr = 0; tItr < n; tItr++) {
            int tItem = Integer.parseInt(tItems[tItr].trim());
            t[tItr] = tItem;
        }
        int index = 0;
        int min = Integer.MAX_VALUE;

        for(int i = 0; i < n; i++){
            int counter = 0;
            int sum = 0;
            for(int j = i; j < n; j++) {
                int add = (t[j] - counter);
                if( add < 0){
                    add = 0;
                }
                System.out.println("i, j, add" + i + " " + j + " " + add);
                sum += add;
                counter++;
                if(j == n-1 && counter != n) {
                    //System.out.println("masuk j :" + j + " i :" + i);
                    j = -1;
                }
                if(j == i-1){
                    break;
                }
            }
             System.out.println(sum );
            if( sum < min ){
                min = sum;
                index = i;
            }
            if( min == 0 ){
                break;
            }

        }
        // System.out.println(index);
        System.out.println(index+1);

    }
}
