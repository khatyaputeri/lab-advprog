package tutorial.javari;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;
import org.springframework.web.bind.annotation.*;

import tutorial.javari.animal.Animal;
import tutorial.javari.animal.Condition;
import tutorial.javari.animal.Gender;

@RestController
public class JavariController {
    private static List<Animal> list = new ArrayList<>();
    private final String path = "animal_javari.csv";
    private final Path file = Paths.get("", path);


    public JavariController() throws IOException {
    }

    @GetMapping("/javari")
    public Object getAllAnimal() {
        if (list == null || list.isEmpty()) {
            return Message.empty();
        }
        return list;
    }

    @GetMapping("/javari/{idAnimal}")
    public Object getAnimal(@PathVariable Integer idAnimal) {
        Animal animal = getAnimalwithId(idAnimal);
        if (animal == null) {
            return Message.notFound(idAnimal);
        }
        return animal;
    }

    @DeleteMapping("/javari/{idAnimal}")
    public Object deleteAnimal(@PathVariable Integer idAnimal) throws IOException {
        Animal animal = deleteAnimalwithId(idAnimal);
        if (animal != null) {
            Object[] message = {Message.successDelete(), animal};
            return message;
        }
        return Message.notFound(idAnimal);
    }

    @PostMapping("/javari")
    public Object insertAnimal(@RequestBody String input) throws IOException {
        Animal animal = addNewAnimal(input);
        Object[] message = {Message.successAdd(), animal};
        return message;
    }

    public Animal getAnimalwithId(int idAnimal) {
        for (Animal animal : list) {
            if (idAnimal == animal.getId()) {
                return animal;
            }
        }
        return null;
    }

    public Animal deleteAnimalwithId(int idAnimal) throws IOException {
        Animal animal = null;
        for (int j = 0; j < list.size(); j++) {
            if (list.get(j).getId() == idAnimal) {
                animal = list.remove(j);
                break;
            }
        }
        this.saveData();
        return animal;
    }

    public Animal addNewAnimal(String animal) throws IOException {
        Animal animalNew = this.convertJsonintoAnimal(animal);
        if (!this.isDuplicate(animalNew)) {
            list.add(animalNew);
            this.saveData();
            return animalNew;
        }
        return null;
    }

    private boolean isDuplicate(Animal animal) {
        for (Animal an : list) {
            if (animal.getId() == an.getId()) {
                return true;
            }
        }
        return false;
    }

    private void loadData() throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(file.toString()));
        String readLine = reader.readLine();

        while (readLine != null) {
            this.list.add(inputToAnimal(readLine));
            readLine = reader.readLine();
        }

        reader.close();
    }

    private void saveData() throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(file.toString()));
        for (Animal an : list) {
            String output = this.animalToOutput(an);
            writer.write(output);
            writer.newLine();
        }
        writer.close();
    }

    private Animal inputToAnimal(String fileInput) {
        String[] listInput = fileInput.split(",");
        Integer idAnimal = Integer.parseInt(listInput[0]);
        String typeAnimal = listInput[1];
        String nameAnimal = listInput[2];
        Double length = Double.parseDouble(listInput[4]);
        Double weight = Double.parseDouble(listInput[5]);
        return new Animal(idAnimal, typeAnimal,
                nameAnimal,
                Gender.parseGender(listInput[3]),
                length, weight,
                Condition.parseCondition(listInput[6])
        );
    }

    private String animalToOutput(Animal animal) {
        String[] component = {animal.getId().toString(), animal.getName(),
                animal.getType(), animal.getGender().toString(),
                String.valueOf(animal.getLength()),
                String.valueOf(animal.getWeight()),
                animal.getCondition().toString()
        };
        return String.join(",", component);
    }

    private Animal convertJsonintoAnimal(String input) {
        JSONObject json = new JSONPObject(input);
        return new Animal(json.getInt("id"), json.getString("type"),
                json.getString("name"), Gender.parseGender(json.getString("gender")),
                json.getDouble("length"), json.getDouble("weight"),
                Condition.parseCondition(json.getString("condition")));
    }

}




