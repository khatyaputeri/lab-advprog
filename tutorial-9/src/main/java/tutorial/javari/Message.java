package tutorial.javari;

public class Message {
    private String type;
    private String message;

    private Message(String type, String message) {
        this.type = type;
        this.message = message;
    }

    public static Message successAdd() {
        return new Message("Success",
                "SUCCESS =>> Animal added : ");
    }

    public static Message successDelete() {
        return new Message("Success",
                "SUCCESS =>> Animal deleted : ");
    }

    public static Message notFound(int id) {
        return new Message("Unknown",
                "ERROR!! No animal found with id = " + id);
    }

    public static Message empty() {
        return new Message("Unknown",
                "ERROR!! Empty List");
    }

    public String getType() {
        return type;
    }

    public String getMessage() {
        return message;
    }


}
